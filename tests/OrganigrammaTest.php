<?php
/**
 * OrganigrammaTest.php
 * Data creazione: 25/03/2020
 */

use Bpf\BaseBundle\Organigramma;
use PHPUnit\Framework\TestCase;

class OrganigrammaTest extends TestCase
{
    private Organigramma $obj;

    protected function setUp(): void
    {
        parent::setUp();
        $cacheDir = __DIR__."\\tmp";
        if (file_exists("$cacheDir\\organigramma.php")) unlink("$cacheDir\\organigramma.php");
        $this->obj = new Organigramma(__DIR__."\\var\\organigramma.json", $cacheDir);
    }

    /**
     * @throws Exception
     */
    public function testFiliale()
    {
        self::assertEquals(99, $this->obj->filiale(223));
        self::assertEquals(156, $this->obj->filiale(173));
        self::assertEquals(807, $this->obj->filiale(133));
    }

    /**
     * @throws Exception
     */
    public function testNomeUfficio()
    {
        self::assertEquals("Sistema informatico e procedure", $this->obj->nomeUfficio("SINF"));
    }

    /**
     * @throws Exception
     */
    public function testUfficiMatricole()
    {
        self::assertCount(56, $this->obj->unitaOrganizzative());
        self::assertCount(142, $this->obj->dipendenti());
    }

    /**
     * @throws Exception
     */
    public function testUsoCache()
    {
        $this->obj->compila(); // Forza la generazione della cache
        // Reinstanzia l'oggetto senza eliminare la cache
        $this->obj = new Organigramma(__DIR__."\\var\\organigramma.json", __DIR__."\\tmp");
        $this->testFiliale();
        $this->testNomeUfficio();
        $this->testUfficiMatricole();
    }

    public function testIsFiliale()
    {
        self::assertTrue(Organigramma::isFiliale("F013"));
        self::assertFalse(Organigramma::isFiliale("LOGE"));
    }
}