<?php

use Bpf\BaseBundle\Filiali;
use PHPUnit\Framework\TestCase;

/**
 * FilialiTest.php
 * Data creazione: 16/11/2021
 */
class FilialiTest extends TestCase
{
    public function testIndirizzoEmail()
    {
        self::assertEquals('fondi@bpfondi.it', Filiali::email(0));
        self::assertEquals('fondi@bpfondi.it', Filiali::email('0'));
    }

    /**
     * @throws Exception
     */
    public function testHubDiRiferimento()
    {
        self::assertEquals(808, Filiali::hubDiRiferimento(123));
        self::assertEquals(807, Filiali::hubDiRiferimento(156));
    }
}