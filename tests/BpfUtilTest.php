<?php
/**
 * BpfUtilTest.php
 * Data creazione: 13/06/2016
 */

use Bpf\BaseBundle\BpfUtil;
use PHPUnit\Framework\TestCase;

class BpfUtilTest extends TestCase
{
    public function testFestivo()
    {
        $festivi = ['2016-06-02', '2016-06-12', '2015-04-06', '2017-01-01', '2016-12-26', '2015-08-15'];
        $lavorativi = ['2016-06-13', '2015-04-07', '2015-12-24'];
        foreach ($festivi as $dt) {
            self::assertTrue(BpfUtil::festivo(strtotime($dt)), "$dt deve essere festivo!");
        }
        foreach ($lavorativi as $dt) self::assertFalse(BpfUtil::festivo(strtotime($dt)));
    }

    public function testGiorniLav()
    {
        $tst = strtotime('2019-02-13 12:00:00');
        $giorni = [
            '12/02/2019' => -1,
            '11/02/2019' => -2,
            '08/02/2019' => -3,
            '14/02/2019' => 1,
            '15/02/2019' => 2,
            '18/02/2019' => 3
        ];
        foreach($giorni as $dt => $gg) self::assertEquals($dt, date('d/m/Y', BpfUtil::giorniLav($gg, $tst)));
    }

    public function testGgLavorativoPrec()
    {
        $giorni = [
            '2019-02-11' => '2019-02-08',
            '2019-02-12' => '2019-02-11',
            '2019-02-13' => '2019-02-12',
            '2019-02-14' => '2019-02-13',
            '2019-02-15' => '2019-02-14',
            '2019-02-16' => '2019-02-15',
            '2019-02-17' => '2019-02-15',
            '2019-02-18' => '2019-02-15'
        ];
        foreach($giorni as $dt => $lavPrec) {
            $tst = strtotime($dt.' 12:00:00');
            self::assertEquals($lavPrec, date('Y-m-d', BpfUtil::ggLavorativoPrec($tst)));
        }
    }
}
