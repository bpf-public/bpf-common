<?php
/**
 * BpfControllerTest.php
 * Data creazione: 31/03/2020
 */

namespace Controller;

use Bpf\BaseBundle\Controller\BpfController;
use Bpf\BaseBundle\Matricole;
use Bpf\BaseBundle\Organigramma;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class BpfControllerTest extends TestCase
{
    public function testFilialeDaIp()
    {
        $obj = new class extends BpfController {
            public function filiale($ipAddr) { return parent::filialeDaIp($ipAddr); }
        };
        self::assertEquals(15, $obj->filiale('10.40.15.2'));
        self::assertEquals(99, $obj->filiale('10.40.99.30'));
        self::assertEquals(0, $obj->filiale('10.40.100.15'));
        self::assertEquals(99, $obj->filiale('::1'));
        self::assertEquals(-1, $obj->filiale('10.40.240.15'));
        self::assertEquals(-1, $obj->filiale('10.13.1.1'));
    }

    public function testRilevaFiliale()
    {
        $obj = new class extends BpfController {
            public function filiale(Request $req, Matricole $matr = null, Organigramma $org = null) {
                return parent::rilevaFiliale($req, $matr, $org);
            }
        };
        $req = new Request();
        $req->server->set('REMOTE_ADDR', '10.40.240.255');
        $req->server->set('REMOTE_USER', 'FOBANCHE\\199');

        // Senza Matricole né Organigramma deve restituire -1
        self::assertEquals(-1, $obj->filiale($req));

        $baseDir = dirname(__DIR__);
        $matr = new Matricole("$baseDir/var/matr_sr.json", "$baseDir/tmp/matr_sr.php");

        // Con matricole deve rilevare filiale corretta
        self::assertEquals(156, $obj->filiale($req, $matr));

        // Con Organigramma deve rilevare la filiale nominale
        $org = new Organigramma("$baseDir/var/organigramma.json", "$baseDir/tmp");
        self::assertEquals(10, $obj->filiale($req, null, $org));
    }
}