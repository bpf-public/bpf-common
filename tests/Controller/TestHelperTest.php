<?php
/**
 * TestHelperTest.php
 * Data creazione: 06/09/2022
 */

namespace Controller;

use Bpf\BaseBundle\Tests\BaseTest;
use Bpf\BaseBundle\Tests\TestHelper;
use Exception;

class TestHelperTest extends BaseTest
{
    /**
     * @throws Exception
     */
    public function testStripHTML()
    {
        $html = <<<'HTML'
<!DOCTYPE html>
<html lang='it'>
<head>
<title>Titolo</title>
<style>BODY { font-family: Calibri, Verdana; }</style>
</head>
<body onclick="alert('ciao')">Prova123&nbsp;spazio &quot;virgolette&quot;<br/>&nbsp;<br/>
<table style="border-collapse:collapse"><tr style="background-color:#DDD;"><th style="font-style:italic;">TH1</th><th 
    style="font-weight:normal;">TH2</th></tr><tr><td style="vertical-align: top" rowspan="2">TD1.1 rowspan2</td>
    <td style="border:1px solid #999;">TD1.2</td></tr><tr><td>TD2.2</td></tr><tr><td>TD3.1</td><td>TD3.2</td></tr>
    </table><br/>Testo <em>finale</em>.</body></html>
HTML;
        $str = 'Prova123 spazio "virgolette" TH1 TH2 TD1.1 rowspan2 TD1.2 TD2.2 TD3.1 TD3.2 Testo finale.';
        self::assertEquals($str, TestHelper::stripHTML($html));
    }
}