<?php

use Bpf\BaseBundle\Matricole;
use PHPUnit\Framework\TestCase;

/**
 * MatricoleTest.php
 * Data creazione: 30/03/2020
 */

class MatricoleTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testFiliale()
    {
        $cacheFile = __DIR__."\\tmp\\matr_sr.php";
        if (file_exists($cacheFile)) unlink($cacheFile);
        $o = new Matricole(__DIR__."\\var\\matr_sr.json", $cacheFile);
        self::assertEquals(99, $o->filiale(223));
    }
}