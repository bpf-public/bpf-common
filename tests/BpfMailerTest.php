<?php

use Bpf\BaseBundle\BpfMailer;
use Bpf\BaseBundle\Tests\BaseTest;
use Bpf\BaseBundle\Tests\BpfMailerTester;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\RawMessage;

/**
 * BpfMailerTest.php
 * Data creazione: 11/08/2021
 */
class BpfMailerTest extends BaseTest
{
    /**
     * @throws Exception
     */
    public function testEmailAllegati()
    {
        $tester = new BpfMailerTester();
        $percorsoFile = __DIR__.'/tmp/prova.txt';
        touch($percorsoFile);
        $tester->emailConAllegato('emanuele.iannone@bpfondi.it', 'Test', 'Prova', $percorsoFile);
        $emails = $tester->getMessages();
        self::assertCount(1, $emails);
        self::assertCount(1, $emails[0]['allegati']);
        self::assertEquals(basename($percorsoFile), array_key_first($allegato = $emails[0]['allegati']));
    }

    /**
     * @throws Exception
     */
    public function testEmailAllegatiConNome()
    {
        $tester = new BpfMailerTester();
        $percorsoFile = __DIR__.'/tmp/prova.txt';
        $nome = 'nuovonome.txt';
        touch($percorsoFile);
        $tester->emailConAllegato('emanuele.iannone@bpfondi.it', 'Test', 'Prova', $percorsoFile, null, $nome);
        $emails = $tester->getMessages();
        self::assertCount(1, $emails);
        self::assertCount(1, $emails[0]['allegati']);
        self::assertEquals($nome, array_key_first($allegato = $emails[0]['allegati']));
    }

    /**
     * @throws Exception
     * @throws \PHPUnit\Framework\MockObject\Exception
     *
     */
    public function testSmtpAlternativo()
    {
        //5 Mail inviate: 4 mail (3 smtp principale + 1 smtp alternativo) + 1 mail di notifica a notifiche batch
        $contaInvocazioni = $this->exactly(BpfMailer::MAX_TENTATIVI_SMTP_PRINC + 2);
        $transportMock = $this->createMock(TransportInterface::class);
        $transportMock
            ->expects($contaInvocazioni)
            ->method('send')
            ->willReturnCallback(
                function ($msg) use ($contaInvocazioni) {
                    $xTransport = $msg->getHeaders()->get("X-Transport");
                    if ($contaInvocazioni->getInvocationCount() <= BpfMailer::MAX_TENTATIVI_SMTP_PRINC) {
                        // Per le prime tre invocazioni verifica che non sia presente l'header X-Transport.
                        self::assertNull($xTransport, "Presenza inattesa dell'header 'X-Transport' durante l'invio della mail usando l'smtp principale");
                        // Inoltre simula che l'invio vada in errore
                        throw new TransportException("Chiamata " . $contaInvocazioni->getInvocationCount());
                    }
                    else {
                        // Dalla terza in poi verifica che l'header sia presente
                        self::assertNotNull($xTransport, "Assenza dell'header 'X-Transport' durante l'invio della mail usando l'smtp alternativo");
                        return null;
                    }
                });
        $mailer = new BpfMailer(new Mailer($transportMock));
        $mailer->email("test@bpfondi.it","Test", "Test invio");
    }
}
