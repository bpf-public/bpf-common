<?php
/**
 * BpfMailer.php
 * Data creazione: 05/02/2019
 */

namespace Bpf\BaseBundle;


use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Throwable;

class BpfMailer
{
    public const MAX_TENTATIVI_SMTP_PRINC = 3;
    public const MAX_TENTATIVI_SMTP_TOT = 6;

    protected MailerInterface $mailer;
    protected string $defaultMail;
    protected ?string $fromUser;
    protected ?LoggerInterface $logger;

    public function __construct(MailerInterface $mailer,
                                string $fromUser = null,
                                string $defaultMail = "emanuele.iannone@bpfondi.it",
                                LoggerInterface $logger = null)
    {
        $this->mailer = $mailer;
        $this->defaultMail = $defaultMail;
        $this->fromUser = $fromUser;
        $this->logger = $logger;
    }

    public function setLogger(?LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @throws BpfMailerException
     */
    public function notifica(string $oggetto, string $msg, $mitt = null, $dest = null, bool $importante = false): void
    {
        $this->email($dest ?: $this->defaultMail, $oggetto, self::htmlBody($msg), $mitt, true, $importante);
    }

    /**
     * @throws BpfMailerException
     */
    public function email($dest, string $oggetto, string $msg, $mittente = null, bool $html = true, bool $importante = false): void
    {
        $this->send($this->getMessage($dest, $oggetto, $msg, $mittente, $html, $importante));
    }

    /**
     * @throws BpfMailerException
     */
    public function emailConAllegato($dest, string $oggetto, string $msg, string $file, $mittente = null, string $nomeFile = null): void
    {
        $allegati = [(empty($nomeFile)? 0 : $nomeFile) => $file];
        $this->emailConAllegati($dest, $oggetto, $msg, $allegati, $mittente);
    }

    /**
     * @throws BpfMailerException
     */
    public function emailConAllegati($dest, string $oggetto, string $msg, array $files, $mittente = null): void
    {
        $m = $this->getMessage($dest, $oggetto, $msg, $mittente);
        foreach($files as $nome => $percorso) $m->attachFromPath($percorso, is_numeric($nome)? null : $nome);
        $this->send($m);
    }

    public static function htmlBody(string $body, string $css = "BODY { font-family: Calibri, Verdana; }"): string
    {
        if (strip_tags($body) == $body) $body = nl2br($body);
        return "<!DOCTYPE html>\n<html lang='it'>\n" .
            "<head>".(empty($css) ? '' : '<style>'.$css.'</style>')."</head>\n".
            "<body>".$body."</body></html>";
    }

    /**
     * @throws BpfMailerException
     */
    protected function send(Email $email): void
    {
        $ultimoErrore = null;
        for($tent = 0; $tent < self::MAX_TENTATIVI_SMTP_TOT; $tent++) {
            // Dopo il terzo tentativo passa al server SMTP alternativo
            if ($tent == self::MAX_TENTATIVI_SMTP_PRINC) {
                $email->getHeaders()->addTextHeader('X-Transport', 'alternative');
                if(!empty($this->logger)) $this->logger->warning("Passaggio all'SMTP alternativo");
            }
            try {
                $this->mailer->send($email);
                break;
            }
            catch (Throwable $err) {
                $ultimoErrore = $err->getMessage();
                if (!empty($this->logger)) {
                    $errTxt = "Errore invio e-mail".(($tent > 1)? " (tentativo $tent)" : "").": $ultimoErrore";
                    $this->logger->warning($errTxt);
                }
                sleep(5);
            }
        }
        if ($tent < self::MAX_TENTATIVI_SMTP_PRINC) return; // email inviata tramite SMTP principale

        // Se siamo qui, l'invio col server principale è fallito e si è passati al server alternativo
        $destinatari = join(', ', array_map(function (Address $dest) {return $dest->getAddress();}, $email->getTo()));
        if ($tent < self::MAX_TENTATIVI_SMTP_TOT) {
            // Messaggio inviato col server alternativo
            $not = "Errore nell'invio di una e-mail tramite SMTP principale. Utilizzato SMTP alternativo.<br/>\r\n".
                "Oggetto e-mail: <em>".$email->getSubject()."</em><br/>".
                "Destinatari: <em>$destinatari</em><br/>".
                "Ultimo errore: <em>$ultimoErrore</em>";
            $msgNot = $this->getMessage($this->defaultMail, "Utilizzo SMTP alternativo", $not);
            $msgNot->getHeaders()->addTextHeader('X-Transport', 'alternative');
            try {
                $this->mailer->send($msgNot);
            }
            catch (Throwable $err) {
                if (!empty($this->logger))
                    $this->logger->error("Errore invio notifica di utilizzo SMTP alternativo: ".$err->getMessage());
            }
        }
        else {
            // Impossibile inviare messaggi né col server principale, né con l'alternativo
            $errTxt = "Errore invio email a $destinatari (dopo ".self::MAX_TENTATIVI_SMTP_TOT." tentativi): $ultimoErrore";
            throw new BpfMailerException($errTxt, 0, $err);
        }
    }

    /**
     * @throws BpfMailerException
     */
    protected function getMessage($dest, string $oggetto, string $msg, $mittente = null, bool $html = true, bool $importante = false): Email
    {
        if (empty($dest)) throw new BpfMailerException("Destinatario email mancante");

        // https://symfony.com/doc/4.4/mailer.html
        $m = (new Email())->subject($oggetto);
        if ($html) $m->html($msg, "utf-8"); else $m->text($msg, "utf-8");

        if (is_array($dest) && isset($dest['To'])) {
            if (is_array($dest['To'])) $m->to(...$dest['To']); else $m->to($dest['To']);
            if (isset($dest['Cc'])) {
                if (is_array($dest['Cc'])) $m->cc(...$dest['Cc']); else $m->cc($dest['Cc']);
            }
            if (isset($dest['Bcc'])) {
                if (is_array($dest['Bcc'])) $m->cc(...$dest['Bcc']); else $m->cc($dest['Bcc']);
            }
        } else if (is_array($dest)) {
            $m->to(...$dest);
        } else {
            $m->to($dest);
        }
        if (!empty($mittente)) {
            if (is_array($mittente)) $m->replyTo($mittente[1].' <'.$mittente[0].'>');
            else $m->replyTo($mittente);
        } else {
            $m->replyTo($this->defaultMail);
        }
        // NOTA: Il campo "from" è obbligatorio, e per Office365 deve corrispondere a quello dell'account di posta
        $m->from(empty($this->fromUser)? $m->getReplyTo()[0] : $this->fromUser);

        if ($importante) $m->priority(Email::PRIORITY_HIGHEST);

        return $m;
    }
}