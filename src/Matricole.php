<?php
/**
 * Matricole.php
 * Data creazione: 30/03/2020
 */

namespace Bpf\BaseBundle;

use Exception;

/**
 * Questa classe suppone l'esistenza di un file json aggiornato quotidianamente con i dati delle matricole SR
 * L'aggiornamento viene effettuato dal batch query varie SRMATBAT
 *
 * Class Matricole
 * @package Bpf\BaseBundle
 */
class Matricole
{
    private string $json;
    private string $cacheFile;
    private array $matricole = [];

    public function __construct($jsonPath, $cachePath)
    {
        $this->json = $jsonPath;
        $this->cacheFile = $cachePath;
    }

    /**
     * @throws Exception
     */
    public function filiale($matr): int
    {
        if (empty($this->matricole)) $this->compila();
        return isset($this->matricole[$matr])? $this->matricole[$matr]['fil'] : -1;
    }

    /**
     * @throws Exception
     */
    private function compila()
    {
        if (!file_exists($this->json)) throw new Exception("JSON matricole sr mancante!");
        $tstJson = filemtime($this->json);
        $tstCache = file_exists($this->cacheFile)? filemtime($this->cacheFile) : 0;
        if ($tstJson < $tstCache) { // Cache aggiornata
            $this->matricole = include($this->cacheFile);
            return;
        }
        $this->matricole = [];

        // Cache non aggiornata, la ricrea
        @unlink($this->cacheFile);
        $matrSR = json_decode(file_get_contents($this->json), true);
        foreach($matrSR as $matr => $dati) {
            // Come filiale prende quella di ultima apertura, tranne nei casi in cui la filiale SRUS
            // è una filiale gerarchicamente superiore (direzione, hub o minihub)
            $fil = (isset($dati['fil_ult']) && (!isset($dati['fil']) || $dati['fil'] < 99))? $dati['fil_ult'] : $dati['fil'];
            $this->matricole[$matr] = [
                'fil' => $fil,
                'nome' => $dati['nome']
            ];
        }
        file_put_contents($this->cacheFile, "<?php\r\nreturn ".var_export($this->matricole, true).";");
    }
}