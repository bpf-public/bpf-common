<?php

namespace Bpf\BaseBundle\Tests;

use Exception;
use Monolog\Logger;
use ReflectionClass;
use ReflectionException;
use Symfony\Bridge\Monolog\Handler\ConsoleHandler;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * TestHelper.php
 * Data creazione: 03/09/2020
 */

class TestHelper
{
    public static function removeFiles($dir, $removeDir = false): void
    {
        if (!is_dir($dir)) {
            if (file_exists($dir)) unlink($dir);
            return;
        }
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object == "." || $object == "..") continue;
            if (is_dir($dir . "\\" . $object))
                self::removeFiles($dir . "\\" . $object, true);
            else
                unlink($dir . "\\" . $object);
        }
        if ($removeDir) rmdir($dir);
    }

    public static function getConsoleLogger($nome = 'console', $verbosity = OutputInterface::VERBOSITY_NORMAL): Logger
    {
        return new Logger($nome, [new ConsoleHandler(new ConsoleOutput($verbosity))]);
    }

    /**
     * @throws ReflectionException
     */
    public static function invokeMethod($obj, $methodName, ...$parameters)
    {
        $reflection = new ReflectionClass(get_class($obj));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invoke($obj, ...$parameters);
    }

    /**
     * @throws ReflectionException
     */
    public static function getProperty($obj, $propertyName)
    {
        $reflection = new ReflectionClass(get_class($obj));
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);
        return $property->getValue($obj);
    }

    /**
     * @throws Exception
     */
    public static function stripHTML(string $html): string
    {
        // Se è presente il blocco '<body></body>' estrae solo quello
        $inizio = stripos($html, '<body');
        if ($inizio !== false) {
            $inizio = strpos($html, '>', $inizio) + 1;
            $fine = stripos($html, '</body>');
            if ($fine === false) throw new Exception("Impossibile rilevare fine blocco BODY");
            $html = substr($html, $inizio, $fine - $inizio);
        }
        // Converte <br/> in ritorni a capo
        $html = preg_replace('#<br/?>#i', "\n", $html);
        // Formatta un po' meglio le tabelle
        $html = preg_replace('#</tr>#i', "</tr>\n", $html);
        $html = preg_replace('#</t[dh]>#i', "\\0\t", $html);
        // Spazi bianchi e tag
        $html = str_replace('&nbsp;', ' ', strip_tags($html));
        // Pulisce i ritorni a capo, i caratteri speciali e gli spazi bianchi
        return preg_replace("#[\r\n\s]+#", " ", htmlspecialchars_decode($html));
    }
}