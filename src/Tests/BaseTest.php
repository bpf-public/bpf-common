<?php
/**
 * BaseTest.php
 * Data creazione: 17/05/2016
 */

namespace Bpf\BaseBundle\Tests;


use ReflectionClass;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Monolog\Logger;
use Symfony\Bridge\Monolog\Handler\ConsoleHandler;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseTest extends KernelTestCase
{
    protected static function getRootDir($dir = null): string
    {
        if (self::$kernel == null) self::bootKernel();
        return self::$kernel->getProjectDir().(empty($dir) ? '' : "\\$dir");
    }

    protected static function get($service): ?object
    {
        if (self::$container == null) self::bootKernel();
        return self::$container->get($service);
    }

    protected static function getTestDir(): string
    {
        return self::getRootDir('tests');
    }

    protected static function getParameter($param)
    {
        if (self::$container == null) self::bootKernel();
        return self::$container->getParameter($param);
    }

    /**
     * @throws ReflectionException
     */
    protected static function invokeMethod($obj, $methodName, ...$parameters)
    {
        return TestHelper::invokeMethod($obj, $methodName, ...$parameters);
    }

    /**
     * @throws ReflectionException
     */
    protected static function getProperty($obj, $propertyName)
    {
        return TestHelper::getProperty($obj, $propertyName);
    }

    protected static function getConsoleLogger($nome = 'console', $verbosity = OutputInterface::VERBOSITY_NORMAL): Logger
    {
        return TestHelper::getConsoleLogger($nome, $verbosity);
    }

    protected static function removeFiles($dir, $removeDir = false)
    {
        TestHelper::removeFiles($dir, $removeDir);
    }
}