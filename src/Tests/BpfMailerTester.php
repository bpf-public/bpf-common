<?php
/**
 * BpfMailerTester.php
 * Data creazione: 20/02/2019
 */

namespace Bpf\BaseBundle\Tests;


use Bpf\BaseBundle\BpfMailer;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Transport\NullTransport;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Header\ParameterizedHeader;

class BpfMailerTester extends BpfMailer
{
    protected array $sent = [];
    protected bool $sendMail;

    public function __construct(Transport $transport = null, string $fromUser = null)
    {
        $this->sendMail = ($transport !== null);
        if ($transport == null) $transport = new NullTransport();
        parent::__construct(new Mailer($transport), $fromUser);
    }

    /**
     * @return Email[]
     */
    public function getMessages(): array
    {
        return $this->sent;
    }

    protected function send(Email $email): void
    {
        if ($this->sendMail) parent::send($email);
        $allegati = [];
        foreach($email->getAttachments() as $attachment) {
            $ct = $attachment->getPreparedHeaders()->get('Content-Type');
            $nome = ($ct instanceof ParameterizedHeader)? $ct->getParameter('name') : '';
            if (!empty($nome)) $allegati[$nome] = $attachment->getBody();
        }
        $destinatari = array_map(function ($addr) { return $addr->getAddress(); }, $email->getTo());
        $cc = array_map(function ($addr) { return $addr->getAddress(); }, $email->getCc());
        $this->sent[] = [
            'subject' => $email->getSubject(),
            'to' => (count($destinatari) > 1)? $destinatari : $destinatari[0],
            'cc' => empty($cc)? null : ((count($cc) > 1)? $cc : $cc[0]),
            'body' => $email->getHtmlBody(),
            'text' => $email->getTextBody(),
            'allegati' => $allegati
        ];
    }
}