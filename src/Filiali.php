<?php
/**
 * Filiali.php
 * Data creazione: 24/12/2014
 */

namespace Bpf\BaseBundle;


use Exception;

class Filiali
{
    const DEFAULT_MAIL_FILIALI = "organizzazione@bpfondi.it";

    public static array $nomi = [
        0  => "Fondi Sede",
        2  => "Lenola",
        3  => "Monte San Biagio",
        4  => "Sperlonga",
        5  => "Fondi - Agenzia 5",
        6  => "Pico",
        7  => "Formia",
        8  => "Latina",
        9  => "Ceccano",
        10 => "Gaeta",
        11 => "Fondi - Agenzia 1",
        12 => "Latina - Borgo Grappa",
        13 => "Pontinia",
        14 => "Fondi - Agenzia 2",
        15 => "Itri",
        16 => "Scauri - Minturno",
        17 => "Castro dei Volsci",
        18 => "Latina - Borgo Sabotino",
        19 => "Fondi - Agenzia 3",
        20 => "Fondi - Agenzia 4",
        21 => "Terracina",
        22 => "Latina - Scalo",
        23 => "Frosinone",
        24 => "Aprilia",
        25 => "Roma",
        99 => "Direzione Generale",
        123 => "Filiali di Pontinia/Borgo Grappa",
        156 => "Filiali di Itri/Pico",
        800 => "Hub Fondi",
        801 => "Hub Storico",
        807 => "Hub Formia",
        808 => "Hub Latina",
        823 => "Hub Frosinone"
    ];

    public static array $filiali = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];

    public static array $filialiSenzaHub = [25];

    public static array $hub = [
        800 => [4, 5, 14],
        801 => [0, 2, 3, 19],
        807 => [7, 10, 16, 6, 15],
        808 => [8, 18, 12, 13, 24],
        823 => [9, 21, 23]
    ];

    public static array $miniHub = [
        123 => [12, 13],
        156 => [6, 15]
    ];

    public static function codici(): array
    {
        return array_keys(self::$nomi);
    }

    public static function getNome($cod): string
    {
        return self::$nomi[$cod] ?? $cod;
    }

    public static function codiciUrl(): array
    {
       return array_map('self::generaUrl', self::$nomi);
    }

    public static function codUrlFiliale($fil): string
    {
        return self::generaUrl(self::$nomi[$fil]);
    }

    private static function generaUrl($nome): string
    {
        return preg_replace("#[-\s]+#", "-", strtolower($nome));
    }

    public static function elenco_alfab(): array
    {
        return [
            "99"  => "DIREZIONE GENERALE",
            "24"  => "APRILIA",
//            "17"  => "CASTRO DEI VOLSCI",
            "9"   => "CECCANO",
//            "11"  => "FONDI AGENZIA 1",
            "14"  => "FONDI AGENZIA 2",
            "19"  => "FONDI AGENZIA 3",
//            "20"  => "FONDI AGENZIA 4",
            "5"   => "FONDI AGENZIA 5",
            "0"   => "FONDI SEDE",
            "7"   => "FORMIA",
            "23"  => "FROSINONE",
            "10"  => "GAETA",
//            "807" => "HUB FORMIA",
//            "808" => "HUB LATINA",
//            "801" => "HUB STORICO",
            "15"  => "ITRI",
            "8"   => "LATINA",
            "12"  => "LATINA - BORGO GRAPPA",
            "18"  => "LATINA - BORGO SABOTINO",
            "22"  => "LATINA - SCALO",
            "2"   => "LENOLA",
            "3"   => "MONTE SAN BIAGIO",
            "6"   => "PICO",
            "13"  => "PONTINIA",
            "25"  => "ROMA",
            "16"  => "SCAURI-MINTURNO",
            "4"   => "SPERLONGA",
            "21"  => "TERRACINA"
        ];
    }

    public static function email(int $num): string
    {
        return match ($num) {
            0 => "fondi@bpfondi.it",
            2 => "lenola@bpfondi.it",
            3 => "montesanbiagio@bpfondi.it",
            4 => "sperlonga@bpfondi.it",
            5 => "fondi5@bpfondi.it",
            6 => "pico@bpfondi.it",
            7 => "formia@bpfondi.it",
            8 => "latina@bpfondi.it",
            9 => "ceccano@bpfondi.it",
            10 => "gaeta@bpfondi.it",
            11 => "fondi5@bpfondi.it",  // "fondi1@bpfondi.it"
            12 => "borgograppa@bpfondi.it",
            13 => "pontinia@bpfondi.it",
            14 => "fondi2@bpfondi.it",
            15 => "itri@bpfondi.it",
            16 => "scauri@bpfondi.it",
            17 => "frosinone@bpfondi.it",  // "castrodeivolsci@bpfondi.it"
            18 => "borgosabotino@bpfondi.it",
            19 => "fondi3@bpfondi.it",
            20 => "fondi5@bpfondi.it",
            21 => "terracina@bpfondi.it",
            22 => "latina@bpfondi.it",   // "latinascalo@bpfondi.it";
            23 => "frosinone@bpfondi.it",
            24 => "aprilia@bpfondi.it",
            25 => "roma@bpfondi.it",
            123 => "borgograppa@bpfondi.it",
            156 => "pico@bpfondi.it",
            800 => "giuseppe.conti@bpfondi.it", // Sarebbe "hubfondi@bpfondi.it", ma da sistemare perché arriva a un sacco di gente;
            801 => "giulio.rasile@bpfondi.it",  // Sarebbe "hubstorico@bpfondi.it", ma da sistemare perché arriva a un sacco di gente;
            807 => "paolo.testa@bpfondi.it",    // Sarebbe "hubformia@bpfondi.it", ma da sistemare perché arriva a un sacco di gente;
            808 => "stanislao.santella@bpfondi.it", // Sarebbe "hublatina@bpfondi.it", ma da sistemare perché arriva a un sacco di gente;
            823 => "giuliano.rea@bpfondi.it",   // Sarebbe "hubfrosinone@bpfondi.it", ma da sistemare perché arriva a un sacco di gente;
            default => self::DEFAULT_MAIL_FILIALI,
        };
    }

    public static function competenti(int $fil): array
    {
        return match ($fil) {
            99 => [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 18, 19, 21, 23, 24, 25],
            6, 15, 156 => self::$miniHub[156],
            12, 13, 123 => self::$miniHub[123],
            800, 801, 807, 808, 823 => self::$hub[$fil],
            default => isset(self::$nomi[$fil]) ? [$fil] : [],
        };
    }

    /**
     * @throws Exception
     */
    public static function hubDiRiferimento(int $fil): int
    {
        // Se è un hub restituisce se stesso
        if (isset(self::$hub[$fil])) return $fil;
        // Se è minihub considera l'hub della prima filiale di pertinenza del minihub
        if (isset(self::$miniHub[$fil])) $fil = self::$miniHub[$fil][0];

        foreach (self::$hub as $codiceHub => $h) {
            if (in_array($fil, $h)) return $codiceHub;
        }
        throw new Exception("Nessun hub di riferimento associato alla filiale $fil");
    }

    /**
     * @throws Exception
     */
    public static function presentaHubDiRiferimento(int $fil): bool
    {
        // Verifica se è un hub o minihub
        if (isset(self::$hub[$fil]) || isset(self::$miniHub[$fil])) return true;
        // Verifica se è una filiale
        if (in_array($fil, self::$filiali)) return !in_array($fil, self::$filialiSenzaHub);

        throw new Exception("Codice filiale '$fil' non riconosciuto");
    }
}