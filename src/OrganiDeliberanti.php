<?php
/**
 * OrganiDeliberanti.php
 * Data creazione: 03/08/2012
 */

namespace Bpf\BaseBundle;


class OrganiDeliberanti {
    // Da SRQS
    private static array $organi_RAS = [
        'DGA'   => "Responsabile Credito Performing",
        'DGB'   => "Responsabile Area Crediti",
        'DGD'   => "Direttore Generale",
        'DGH'   => "Consiglio di Amministrazione",
        'F7773' => "Capo Zona Latina Sud",
        'F7783' => "Capo Zona Fondi",
        'F8003' => "Responsabile Hub Fondi",
        'F8013' => "Responsabile Hub Storico",
        'F8073' => "Responsabile Hub Formia",
        'F8083' => "Responsabile Hub Latina",
        'F8233' => "Responsabile Hub Frosinone",
        'F1233' => "Responsabile B.go Grappa - Pontinia",
        'F1563' => "Responsabile Itri - Pico"
    ];

    // Da TGOS
    private static array $organi_FIDI = [
        'C.D' => "Comitato di Direzione",
        'C.E' => "Comitato Esecutivo",
        'CAA' => "Direzione Affari",
        'CDA' => "Consiglio di Amministrazione",
        'CDE' => "Consigliere Delegato",
        'COR' => "Responsabile Fidi Corporate",
        'CPF' => "Responsabile Credito Performing",
        'CQSP' => "Automatismo Cessione del quinto",
        'CZ1' => "Capo Zona Fondi",
        'CZ2' => "Capo Zona LT Nord",
        'CZ3' => "Capo Zona LT Sud",
        'DGA' => "Responsabile Area Crediti",
        'DGE' => "Direttore Generale",
        'DGT' => "Direttore Generale",
        'FDG' => "Responsabile Fidi D.G.",
        'FID' => "Addetto Fidi",
        'H00' => "Responsabile Hub Fondi",
        'H01' => "Responsabile Hub Storico",
        'H07' => "Responsabile Hub Formia",
        'H08' => "Responsabile Hub Latina",
        'H23' => "Responsabile Hub Frosinone",
        'IST' => "Responsabile Ufficio istruttoria",
        'RET' => "Responsabile Fidi Retail",
        'SPA' => "Responsabile Spoke A",
        'SPB' => "Responsabile Spoke B",
        'TDA' => "Responsabile Dipendenza A",
        'TDB' => "Responsabile Dipendenza B",
        'TDC' => "Responsabile Dipendenza C",
        'VDG' => "Vice Direttore Generale",
        'VTD' => "Vice Titolare Dipendenza"
    ];

    /**
     * Restituisce true se l'organo è di Filiale
     */
    public static function isFiliale(string $org): bool
    {
        return in_array($org, ['TDA','TDB','TDC','VTD','SPA','SPB','FID'])
            || (substr($org, 0, 2) == 'F0');
    }

    public static function descr(string $org, bool $RAS = false) : string
    {
        if ($RAS && isset(self::$organi_RAS[$org])) return self::$organi_RAS[$org];
        else if (isset(self::$organi_FIDI[$org])) return self::$organi_FIDI[$org];

        // Filiali RAS, in formato FXXX3
        if (substr($org, 0, 1) == 'F') {
            $fil = intval(substr($org, 1, 3));
            if (isset(Filiali::$nomi[$fil])) return 'Responsabile Filiale di '.Filiali::$nomi[$fil];
        }

        if (in_array(substr($org, 0, 2), ['TD','SP','VT']) && strlen($org) > 5) {
            $fil = intval(substr($org, -3));
            if (isset(Filiali::$nomi[$fil])) return 'Responsabile Filiale di '.Filiali::$nomi[$fil];
        }
        return '???';
    }

    public static function gerarchia(): array
    {
        $out = ['CDA', 'DGH', 'C.E', 'CDE', 'C.D',
            'DGD', 'DGT', 'DGE', 'DGE(V)', 'VDG',
            'CAA', 'DGB', 'DGA',
            'CPF', 'FDG', 'COR', 'RET',
            'H00', 'H01', 'H07', 'H08', 'H23',
            'F8003', 'F8013', 'F8073', 'F8083', 'F8233',
            'CZ1', 'CZ2', 'CZ3', 'F7773', 'F7783'];
        $cod_filiali = array_keys(Filiali::$nomi);
        foreach($cod_filiali as $cod) {
            $cod3 = sprintf("%03d", $cod);
            $out[] = 'TDA-'.$cod3;
            $out[] = 'TDB-'.$cod3;
            $out[] = 'TDC-'.$cod3;
            $out[] = 'SPA-'.$cod3;
            $out[] = 'SPB-'.$cod3;
            $out[] = 'VTD-'.$cod3;
            $out[] = 'F'.$cod3.'3';
            $out[] = 'FID-'.$cod3;
        }
        $out[] = 'F1233';
        $out[] = 'F1563';
        return $out;
    }
}
