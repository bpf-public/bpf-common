<?php
/**
 * Organigramma.php
 * Data creazione: 25/03/2020
 */

namespace Bpf\BaseBundle;


use Exception;

class Organigramma
{
    private string $jsonFile;
    private string $cacheFile;
    private array $filiali = [];
    private array $uffici = [];
    private array $matricole = [];

    public function __construct(string $json_organigramma, string $cache_dir)
    {
        $this->jsonFile = $json_organigramma;
        $this->cacheFile = $cache_dir."/organigramma.php";
    }

    /**
     * @throws Exception
     */
    public function filiale($matr): int
    {
        if (empty($this->filiali)) $this->compila();
        return empty($matr)? -1 : ($this->filiali[$matr] ?? -1);
    }

    /**
     * @throws Exception
     */
    public function nomeUfficio($codiceUfficio): string
    {
        if (empty($this->uffici)) $this->compila();
        return empty($codiceUfficio)? "[]" : ($this->uffici[$codiceUfficio] ?? "[$codiceUfficio]");
    }

    /**
     * @throws Exception
     */
    public function unitaOrganizzative(): array
    {
        if (empty($this->uffici)) $this->compila();
        return $this->uffici;
    }

    /**
     * @throws Exception
     */
    public function dipendenti(): array
    {
        if (empty($this->matricole)) $this->compila();
        return $this->matricole;
    }

    /**
     * @throws Exception
     */
    public function nomeMatricola(int $matr): string
    {
        if (empty($this->matricole)) $this->compila();
        return empty($matr)? "[]" : ($this->matricole[$matr] ?? "[$matr]");
    }

    /**
     * @throws Exception
     */
    public function compila(): void
    {
        if (!file_exists($this->jsonFile)) throw new Exception("JSON Organigramma mancante!");
        $tstJson = filemtime($this->jsonFile);
        $tstCache = file_exists($this->cacheFile)? filemtime($this->cacheFile) : 0;
        if ($tstJson < $tstCache) { // Cache aggiornata
            [$this->filiali, $this->uffici, $this->matricole] = include($this->cacheFile);
            return;
        }
        // Cache non aggiornata, la ricrea
        @unlink($this->cacheFile);
        $organigramma = json_decode(file_get_contents($this->jsonFile), true);
        foreach($organigramma as $cod => $dati) {
            $this->uffici[$cod] = $dati['nome'];
            if (!isset($dati['membri'])) continue;
            $fil = self::filialeUO($cod);
            if ($fil == -1) continue;
            foreach($dati['membri'] as $matr => $nome) {
                $this->matricole[intval($matr)] = $nome;
                $this->filiali[intval($matr)] = $fil;
            }
        }
        asort($this->matricole);
        file_put_contents($this->cacheFile, "<?php\r\nreturn [".var_export($this->filiali, true).','.
            var_export($this->uffici, true).','.var_export($this->matricole, true).'];');
    }

    /**
     * Ottiene la filiale corrispondente all'unità organizzativa
     */
    private static function filialeUO(string $codUO): int
    {
        if ($codUO == 'CDAM') return -1; // Consiglio di amministrazione
        if (preg_match('#^([F|H])(\d+)$#', $codUO, $m)) return (($m[1] == 'F')? 0 : 800) + intval($m[2]);
        return 99;
    }
    public static function isFiliale(string $codUO): bool
    {
        return preg_match('#^F\d{3}$#', $codUO) == 1;
    }
}