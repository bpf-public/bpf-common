<?php
/**
 * BpfUtil.php
 * Data creazione: 13/06/2016
 */

namespace Bpf\BaseBundle;


class BpfUtil
{
    public static function festivo($tst = 0): bool
    {
        static $ggFestivi = ['01-01', '06-01', '25-04', '01-05', '02-06', '15-08', '01-11', '08-12', '25-12', '26-12'];
        if ($tst == 0) $tst = time();
        list($gSett, $gAnno, $anno) = explode(' ', date("w d-m Y", $tst));
        if ($gSett == 0 || $gSett == 6 || in_array($gAnno, $ggFestivi)) return true;
        return ($gSett == 1 && $gAnno == date('d-m', easter_date((int)$anno) + 129600)); // Pasquetta
    }

    public static function giorniLav($gg, int $tst = 0): int
    {
        if ($tst == 0) $tst = time();
        $diffG = ($gg < 0)? -86400 : 86400;
        $ng = abs($gg);
        while($ng > 0) {
            $tst += $diffG;
            if (!self::festivo($tst)) $ng--;
        }
        return $tst;
    }

    public static function ggLavorativoPrec($tst = 0): int
    {
        return self::giorniLav(-1, $tst);
    }

    public static function impFloat($str): float
    {
        return floatval(str_replace(',', '.', str_replace('.', '', $str)));
    }

    public static function floatImp(float $imp, $dec = 2): string
    {
        return number_format($imp, $dec, ',', '.');
    }
}
