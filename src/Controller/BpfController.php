<?php
/**
 * BpfController.php
 * Data creazione: 16/01/2015
 */

namespace Bpf\BaseBundle\Controller;


use Bpf\BaseBundle\Filiali;
use Bpf\BaseBundle\Matricole;
use Bpf\BaseBundle\Organigramma;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class BpfController extends AbstractController
{
    protected function flashMsg($msg)
    {
        $this->get('session')->getFlashBag()->add('msg', $msg);
    }

    protected function getFlashMsg(): string
    {
        $msg = $this->get('session')->getFlashBag()->get('msg');
        return empty($msg) ? '' : $msg[0];
    }

    protected function redirectWithMsg($route, $msg, $parameters = []): RedirectResponse
    {
        $this->flashMsg($msg);
        return $this->redirect($this->generateUrl($route, $parameters));
    }

    /**
     * @throws Exception
     */
    protected static function rilevaFiliale(Request $req, Matricole $matricole = null, Organigramma $organigramma = null): int
    {
        $fil = self::filialeDaIp($req->getClientIp());
        if ($fil == -1) {
            $remote_user = $req->server->get('REMOTE_USER');
            $domPos = strpos($remote_user, "\\");
            if ($domPos !== false) {
                $matr = substr($remote_user, $domPos + 1);
                if ($matricole != null) $fil = $matricole->filiale($matr);
                if ($fil == -1 && $organigramma != null) $fil = $organigramma->filiale($matr);
            }
        }
        return $fil;
    }

    /**
     * @throws Exception
     */
    protected static function filialiCompetenti(Request $req, Matricole $matricole = null, Organigramma $organigramma = null) : array
    {
        return Filiali::competenti(self::rilevaFiliale($req, $matricole, $organigramma));
    }

    protected static function filialeDaIp($ipAddr): int
    {
        if (strpos($ipAddr, '::1') !== false) return 99;
        if (substr($ipAddr, 0, 5) != '10.40') return -1;
        $ip = explode(".", $ipAddr);
        if ($ip[2] == 100) return 0;
        if ($ip[2] > 100) return -1;
        return intval($ip[2]);
    }
}